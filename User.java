package selenium;
import java.lang.IllegalStateException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

@SuppressWarnings("unused")
public class User {
	WebDriver driver;
	//launch chrome browser and open the site http://karutap-dev.geoglyph.net/
	public void launchbrowser() {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\kumar\\Downloads\\chromedriver.exe") ;
        driver = new ChromeDriver();
        driver.get("http://karutap-dev.geoglyph.net");
     	driver.manage().window().maximize(); // This command used to maximize the browser size
        driver.findElement(By.cssSelector("#homelink > div > div.MuiBox-root.css-q9wpf5 > div.MuiGrid-root.appBarloginBtn.css-rguivu")).click();
	}
	
	
	
	
	//To enter the login credentials
	public void login() throws InterruptedException {
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("#loginpage > div.MuiGrid-root.MuiGrid-container.MuiGrid-item.MuiGrid-spacing-xs-undefined.MuiGrid-grid-xs-10.MuiGrid-grid-sm-5.MuiGrid-grid-md-5.MuiGrid-grid-lg-4.MuiGrid-grid-xl-4.content.css-1nril8w > div.subContent > form > div:nth-child(1) > input[type=text]")).sendKeys("student1@gmail.com");
		driver.findElement(By.cssSelector("#loginpage > div.MuiGrid-root.MuiGrid-container.MuiGrid-item.MuiGrid-spacing-xs-undefined.MuiGrid-grid-xs-10.MuiGrid-grid-sm-5.MuiGrid-grid-md-5.MuiGrid-grid-lg-4.MuiGrid-grid-xl-4.content.css-1nril8w > div.subContent > form > div:nth-child(2) > input[type=password]")).sendKeys("Test@123");
		driver.findElement(By.className("loginButton")).click();
		
	}
	//To change the profile image the image is set in the path
	public void changeProfileImage () throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("#root > div.MuiGrid-root.homeBackGround.css-rfnosa > grid > grid > div > div:nth-child(3) > div > text")).click();
		driver.findElement(By.cssSelector("#root > div.MuiGrid-root.userprofileBackground.css-rfnosa > div.MuiGrid-root.MuiGrid-container.MuiGrid-spacing-xs-undefined.userprofileContainer.css-1d3bbye > div.MuiGrid-root.MuiGrid-container.MuiGrid-spacing-xs-2.userProfileMainContainer.css-isbt42 > div.MuiGrid-root.MuiGrid-grid-xs-12.MuiGrid-grid-sm-12.MuiGrid-grid-md-3.MuiGrid-grid-lg-3.MuiGrid-grid-xl-3.profileDiv.css-14zkvn0 > div.MuiGrid-root.editProfileButton.editProfileButtonBar.css-rfnosa")).click();
		WebElement chooseFile = driver.findElement(By.cssSelector("#contained-button-file")); //To select choose file button
		chooseFile.sendKeys("C:\\Users\\kumar\\OneDrive\\Pictures\\For Selenium\\Profile Image.jpg"); // To select the file which need to upload
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("body > div.MuiModal-root.MuiDialog-root.teamFormation_dialog_container.css-126xj0f > div.MuiDialog-container.MuiDialog-scrollPaper.css-ekeie0 > div > div > div.MuiDialogActions-root.MuiDialogActions-spacing.css-14b29qc > div > div:nth-child(2)")).click();	
	    Thread.sleep(1000);
	    // This command for come back to home page
	    driver.findElement(By.cssSelector("#root > div.MuiGrid-root.userprofileBackground.css-rfnosa > div.MuiGrid-root.MuiGrid-container.MuiGrid-spacing-xs-undefined.userprofileContainer.css-1d3bbye > div.MuiGrid-root.MuiGrid-grid-xs-12.userprofileHeader.css-1uqhpru > svg")).click();
	    Thread.sleep(500);
	}
	
	//To play singlePlayer game
	public void singlePlayer () throws InterruptedException {
		Thread.sleep(500);
	driver.findElement(By.cssSelector("#root > div.MuiGrid-root.homeBackGround.css-rfnosa > grid > grid > div > div:nth-child(2) > grid > text")).click();//To select game
	driver.findElement(By.cssSelector("#root > div.MuiGrid-root.homeBackGround.css-rfnosa > div.MuiGrid-root.userGameHomeContainer.css-rfnosa > div.MuiGrid-root.KarutaUserHome.css-rfnosa > div > div:nth-child(1) > div > text")).click();// To select singleplayer
   // driver.findElement(By.cssSelector("#root > div.MuiGrid-root.multiplayerChooseKarutaBackGround.css-rfnosa > div.MuiGrid-root.MuiGrid-container.MuiGrid-spacing-xs-2.multiplayerChooseKarutaContainer.css-isbt42 > div.MuiGrid-root.MuiGrid-grid-xs-12.karutaListHeader.css-1uqhpru > svg")).click();// To go onestep back
  //  driver.findElement(By.cssSelector("#root > div.MuiGrid-root.homeBackGround.css-rfnosa > div.MuiGrid-root.userGameHomeContainer.css-rfnosa > div.MuiGrid-root.MuiGrid-grid-xs-12.userBackHeader.css-1uqhpru > svg")).click();//To go onestep back
    driver.findElement(By.cssSelector("#root > div.MuiGrid-root.multiplayerChooseKarutaBackGround.css-rfnosa > div.MuiGrid-root.css-rfnosa > div.MuiGrid-root.css-rfnosa > div > div.headerRowDiv2 > div > div > div.notifyDiv > div > div:nth-child(1) > svg")).click();// To go back homepage
	}
	
	// To create new card in karuta
	public void createCard () throws InterruptedException {
		Thread.sleep(1000);
		// To click the karuta button
		driver.findElement(By.cssSelector("#root > div.MuiGrid-root.homeBackGround.css-rfnosa > grid > grid > div > div:nth-child(1) > div > text")).click();
				
		//suresh anna code
		//Thread.sleep(1000);
//		ArrayList<WebElement> karutaListElements = (ArrayList<WebElement>) driver.findElements(By.className("karutaNameNam"));
//		System.out.println("Karuta List Size ==>"+karutaListElements.size()+"<==");
//		for(WebElement we : karutaListElements) {
//		 System.out.println("Name-->"+we.getText()+"<---"); 
//       karutaListElements.get(1).click();
		
		
	// To select the Card in karuta List
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath("//div[contains(@class, 'karutaNameNam') and text()='Automation Karutap']"));
		element.click();
		
		//To click create karuta button
		driver.findElement(By.className("addIcon")).click();
		
		//To click Add Image Button and add image using id of inputTag
		driver.findElement(By.xpath("//input[@id='contained-button-file']")).sendKeys("C:\\Users\\kumar\\OneDrive\\Pictures\\For Selenium\\First Card.jpg");
		
		//To click addText 
		WebElement addText = driver.findElement(By.xpath("//div[contains(@class, 'createCardAddText') and text()='ADD TEXT']"));
		addText.click();
		
		//To click text Area feild and Add text 
		driver.findElement(By.xpath("//textarea[@id='filled-basic']")).sendKeys("Black Panther");
		//To click OK
		driver.findElement(By.xpath("//div[contains(@class, 'createCardAddTextOkBtnDiv')]")).click();
		
		//To click Add Audio and select the audio file
        driver.findElement(By.xpath("//input[@id= 'contained-button-file1']")).sendKeys("C:\\Users\\kumar\\Music\\black and iron.mp3");
        
        //To Click Play Button
        driver.findElement(By.xpath("//div[contains(@class, 'playIconDiv']")).click();	 
	   
	   
	}
	
	

	public static void main (String[] args) throws InterruptedException {
		User openSite = new User();
		openSite.launchbrowser();
		openSite.login();
		//openSite.changeProfileImage();
		//openSite.singlePlayer();
		openSite.createCard();
	}
}
